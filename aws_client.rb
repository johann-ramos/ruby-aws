#!/usr/bin/env ruby
require 'aws-sdk'
require 'yaml'
require 'net/http'
require 'uri'
require 'pp'

require './app/helpers/struct_helper'

class AwsClient

  def initialize(accesKey, secretAccess)
    @arns = StructHelper.to_ostruct(YAML.load_file('config/arn.yml'))
    @devicefarm = Aws::DeviceFarm::Client.new(
        region: 'us-west-2',
        access_key_id: accesKey,
        secret_access_key: secretAccess
    )
    @platform_type = {
      :ios => 'IOS_APP',
      :android => 'ANDROID_APP',
      :testng => 'APPIUM_JAVA_TESTNG_TEST_PACKAGE'
    }
  end

  def list_projects
    reply = @devicefarm.list_projects({
      })
    reply.projects.each do |e|
      puts "#{e.name} => #{e.arn}"
      list_pools(e.arn)
    end
  end

  def list_pools(arn)
    reply = @devicefarm.list_device_pools({
      arn: arn
      })
    reply.device_pools.each do |e|
      puts "#{e.name}(#{e.type}) => #{e.arn}"
    end
  end

  def validate_upload_platform_type(type)
    begin
      if !@platform_type.include?(type.to_sym)
        raise ArgumentError.new('CREATE UPLOAD EXCEPTION: unrecognized test type')
      end
    rescue ArgumentError => e
      puts e.message
      exit 1
    end
  end


  def create_upload(appName,type,file,pool)
    validate_upload_platform_type(type)
    basename = File.basename file
    platform = @platform_type[type.to_sym]
    reply = @devicefarm.create_upload({
      name: basename,
      type: platform,
      project_arn: @arns.excalibur.project
    })
    upload_callback = reply.upload
    curl_shell(upload_callback.url, file)
    test_scheduler(appName,upload_callback.arn,platform,pool)
  end


  def curl_shell(url,path)
    puts 'Uploading Artifact'
    value = %x{ curl -T #{path} \"#{url}\" }
    puts value
    sleep 1
  end

  def test_scheduler(appName,appArn,platform,pool)

    if platform == 'IOS_APP'
      testing_type = "BUILTIN_FUZZ"
      case pool
        when "ios"
          devices = @arns.excalibur.ios_pool
        when "kiosk"
          devices = @arns.excalibur.kiosk_pool
      end
    else
      testing_type = "BUILTIN_EXPLORER"
      devices = @arns.excalibur.android_pool
    end

    reply = @devicefarm.schedule_run({
      name: appName,
      device_pool_arn: devices,
      project_arn: @arns.excalibur.project,
      app_arn: appArn,
      test: {
        type: testing_type
      },
    })

    runArn = reply.run.arn
    execution_loop(runArn)
  end

  def execution_loop(runArn)
    puts %x{ echo 'RUNNING TESTS'}
    while true
      run = get_run(runArn)
      break if run[:status] == "COMPLETED"
      puts %x{ echo "Running: \"#{run[:type]}\" - \"#{Time.now}\""}
      sleep 300
    end
    puts 'FINISH TESTING'
    pp get_run(runArn)
    if get_run(runArn)[:result] == "PASSED"
      puts %x{ echo 'SUCCESS'}
    else
      abort('FAILED')
      puts %x{ echo 'FAILED'}
    end
  end

  def get_run(runArn)
    reply = @devicefarm.get_run({
      arn: runArn
    })
    status = {
      :type => reply.run.type.to_s,
      :platform => reply.run.platform.to_s,
      :created => reply.run.created.to_s,
      :status => reply.run.status.to_s,
      :result => reply.run.result.to_s,
      :started => reply.run.started.to_s,
      :stopped => reply.run.stopped.to_s,
      :message => reply.run.message.to_s,
      :countersTotal => reply.run.counters.total.to_s,
      :countersPassed => reply.run.counters.passed.to_s,
      :countersFailed => reply.run.counters.failed.to_s,
      :countersWarned => reply.run.counters.warned.to_s,
      :countersErrored => reply.run.counters.errored.to_s,
      :countersStopped => reply.run.counters.stopped.to_s,
      :countersSkipped => reply.run.counters.skipped.to_s,
    }
  end

end
