require 'ostruct'

module StructHelper
  class << self

    def to_ostruct(hash)
      OpenStruct.new(hash.each_with_object({}) do |(key, val), memo|
        memo[key] = val.is_a?(Hash) ? to_ostruct(val) : val
      end)
    end

  end
end
