#!/usr/bin/env ruby
require './aws_client.rb'
require 'optparse'

def main
  options = {
    :access_key => nil,
    :secret_access => nil,
    :appname => nil,
    :type => nil,
    :filepath => nil,
    :pool => nil
  }

  parser = OptionParser.new do|opts|
    opts.banner = "Usage: devicefarm_runner.rb [options]"
    opts.on('-a', '--access_key access', 'Access Key Id') do |aci|
      options[:access_key] = aci;
    end

    opts.on('-s', '--secret_access secret', 'Secret Access Key') do |sak|
      options[:secret_access] = sak;
    end

    opts.on('-n', '--name name', 'App Name') do |name|
      options[:appname] = name;
    end

    opts.on('-t', '--testing_type type', 'Testing Type ios,android,testng') do |t|
      options[:type] = t;
    end

    opts.on('-f', '--file file', 'Artifact File Path') do |a|
      options[:filepath] = a;
    end

    opts.on('-p', '--pool pool', 'Device Pool') do |a|
      options[:pool] = a;
    end

    opts.on('-h', '--help', 'Displays Help') do
      puts opts
      exit
    end
  end

  parser.parse!

  if options[:access_key] == nil
    print 'Enter Access Key Id: '
      options[:access_key] = gets.chomp
  end

  if options[:secret_access] == nil
    print 'Enter Secret Access Key: '
      options[:secret_access] = gets.chomp
  end

  if options[:appname] == nil
    print 'Enter App Name: '
      options[:appname] = gets.chomp
  end

  if options[:type] == nil
    print 'Enter Type of testing: '
      options[:type] = gets.chomp
  end

  if options[:filepath] == nil
    print 'Enter File to tests: '
      options[:filepath] = gets.chomp
  end

  if options[:pool] == nil
    print 'Enter Device Pool Name: '
      options[:pool] = gets.chomp
  end

  callClient(options[:access_key],options[:secret_access],options[:appname],options[:type],options[:filepath],options[:pool])
end

def callClient(accessKey,secretAccess,appName,type,file,pool)
  slot = AwsClient.new(accessKey,secretAccess)
  slot.list_projects
  slot.create_upload(appName,type,file,pool)
end

main()
