#!/usr/bin/env ruby
require './aws_client.rb'
require 'optparse'

def main
  options = {
    :access_key => nil,
    :secret_access => nil,
  }

  parser = OptionParser.new do|opts|
    opts.banner = "Usage: devicefarm_runner.rb [options]"
    opts.on('-a', '--access_key access', 'Access Key Id') do |aci|
      options[:access_key] = aci;
    end

    opts.on('-s', '--secret_access secret', 'Secret Access Key') do |sak|
      options[:secret_access] = sak;
    end

    opts.on('-h', '--help', 'Displays Help') do
      puts opts
      exit
    end
  end

  parser.parse!

  if options[:access_key] == nil
    print 'Enter Access Key Id: '
      options[:access_key] = gets.chomp
  end

  if options[:secret_access] == nil
    print 'Enter Secret Access Key: '
      options[:secret_access] = gets.chomp
  end
  callClient(options[:access_key],options[:secret_access])
end

def callClient(accessKey,secretAccess)
  slot = AwsClient.new(accessKey,secretAccess)
  slot.list_projects
end

main()
