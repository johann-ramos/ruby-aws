# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This scripts emulates the devicefarm plugin which has no Jenkinsfile compatibility.
* 1.0

### How do I get set up? ###

1. How to run locally

  * Clone the repo

  * Make sure you have ruby, rvm or rbenv installed on your system

    * RVM: https://rvm.io/rvm/install
    * RBENV: https://github.com/rbenv/rbenv

  * Install bundler gem: ``` gem install bundler ```

  * Run the following command to install the dependencies: ``` bundle ```

  * Check the arns returned by the ```get_arns.rb``` script. This script has the following options:
```
Usage: get_arns.rb [options]
    -a, --access_key access          Access Key Id
    -s, --secret_access secret       Secret Access Key
    -h, --help                       Displays Hel
```
  Compare the result with the arns stored in ```config/arn.yml```, and make sure the arns are correct.

  * Once you have all the arns configured, run ```devicefarm_runner.rb``` script with the following options:
```
Usage: devicefarm_runner.rb [options]
    -a, --access_key access          Access Key Id
    -s, --secret_access secret       Secret Access Key
    -n, --name name                  App Name
    -t, --testing_type type          Testing Type (ios,android,testng)
    -f, --file file                  Artifact File Path
    -p, --pool pool                  Device Pool (ios, android, kiosk)
    -h, --help                       Displays Help
```

  * When the script finished you should see something like this:
```
Uploading Artifact
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 20944    0     0  100 20944      0  13887  0:00:01  0:00:01 --:--:-- 13888

Starting test
Running: BUILTIN_FUZZ - 2017-11-27 21:37:36 -0300
Running: BUILTIN_FUZZ - 2017-11-27 21:42:37 -0300
FINISH TESTING
{:type=>"BUILTIN_FUZZ",
 :platform=>"IOS_APP",
 :created=>"2017-11-27 21:37:35 -0300",
 :status=>"COMPLETED",
 :result=>"PASSED",
 :started=>"",
 :stopped=>"",
 :message=>"",
 :countersTotal=>"6",
 :countersPassed=>"6",
 :countersFailed=>"0",
 :countersWarned=>"0",
 :countersErrored=>"0",
 :countersStopped=>"0",
 :countersSkipped=>"0"}
SUCCESS
```

2. How to add in Jenkinsfile

  * Use the following variables in you Jenkinsfile:

```
def appName = ""
def type = ""
def file = ""
def pool = ""
```

  * Keep the repo's checkouts inside a separate directory:
```
dir('folder_name')
```

  * Add the following function to your Jenkinsfile:
```
def withRvm(Closure stage) {
  rubyVersion = 'ruby-2.4.2'
  rvmGemset = 'ruby-aws'
  RVM_HOME = "$HOME/.rvm"

  paths = [
      "$RVM_HOME/gems/$rubyVersion@$rvmGemset/bin",
      "$RVM_HOME/gems/$rubyVersion@global/bin",
      "$RVM_HOME/rubies/$rubyVersion/bin",
      "$RVM_HOME/bin",
      "${env.PATH}"
  ]

  env.PATH = paths.join(':')
  env.GEM_HOME = "$RVM_HOME/gems/$rubyVersion@$rvmGemset"
  env.GEM_PATH = "$RVM_HOME/gems/$rubyVersion@$rvmGemset:$RVM_HOME/gems/$rubyVersion@global"
  env.MY_RUBY_HOME = "$RVM_HOME/rubies/$rubyVersion"
  env.IRBRC = "$RVM_HOME/rubies/$rubyVersion/.irbrc"
  env.RUBY_VERSION = "$rubyVersion"

  stage()
}
```

  * Finally you can add the full devicefarm stage:
```
  stage('devicefarm'){
    dir('devicefarm'){
      withRvm(){
        checkout([
          $class: 'GitSCM',
          branches: [[name: '*/master']],
          doGenerateSubmoduleConfigurations: false,
          extensions: [],
          submoduleCfg: [],
          userRemoteConfigs: [[
            credentialsId: '',
            url: ''
            ]]
        ])
        sh 'which bundle || gem install bundler'
        sh 'bundle install'

        withCredentials([[
          $class: 'AmazonWebServicesCredentialsBinding',
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',
          credentialsId: 'devicefarm',
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY'
        ]]) {

          sh " ruby devicefarm_runner.rb --access_key $AWS_ACCESS_KEY_ID --secret_access $AWS_SECRET_ACCESS_KEY --name $appName --testing_type $type --file $file"
        }
      }
    }
  }
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Johann Ramos <johann.ramos@globant.com> - <jramos@rccl.com>
